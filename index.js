import express from 'express'
import compression from 'compression'
import robots from 'express-robots-txt'
import cors from 'cors'
import path from 'path'
import { fileURLToPath } from 'url'
import { readJsonSync } from 'fs-extra/esm'
import fg from 'fast-glob'
import * as dotenv from 'dotenv'

dotenv.config()

const ENV_PORT = process.env.PORT || 3000
const ENV_REQUEST_DELAY = process.env.REQUEST_DELAY || 100

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const __routesPath = path.normalize(`${__dirname}/routes`)
const __basePath = path.normalize(`${__routesPath}/**/*.json`)

let ENV_COMPRESS = process.env.COMPRESS || 'true'
ENV_COMPRESS = ENV_COMPRESS === 'true'

const app = express()

function delayedGet(app, entry) {
  const relPath = path.relative(__routesPath, entry)
  const route = relPath.substr(0, relPath.lastIndexOf('.'))

  console.log(route)

  const parseURL = new URL(`http://x.de/${route}`)
  const pathname = parseURL.pathname
  const search = parseURL.search

  app.get(pathname, function (req, res) {
    if (search) entry = rebuildPath(req.url)
    const { parsedJsonObj, delay } = getData(entry)
    if (delay > 0) console.log(`DELAYED:  ${delay}ms`)
    setTimeout(() => {
      res.json(parsedJsonObj)
    }, delay)
  })
}

function getData(entry) {
  let parsedJsonObj = {}
  let delay = 0
  try {
    parsedJsonObj = readJsonSync(entry)
    delay = parsedJsonObj.__delayed || ENV_REQUEST_DELAY
  } catch (e) {
    console.error(`CANNOT CREATE / invalid JSON : ${entry}`)
    console.error(e)
  }
  return {
    parsedJsonObj,
    delay
  }
}

function rebuildPath(routeRelative) {
  return path.normalize(`${__routesPath}/${routeRelative}.json`)
}

if (ENV_COMPRESS) app.use(compression())

console.log()
console.log('# START SERVER')
console.log()
console.log('.env:')
console.log(`PORT=${ENV_PORT}`)
console.log(`REQUEST_DELAY=${ENV_REQUEST_DELAY}`)
console.log(`COMPRESS=${ENV_COMPRESS}`)
console.log()

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET')
  res.header(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, accept, access-control-allow-origin'
  )
  if (req.method === 'OPTIONS') res.send(200)
  else next()
})

app.use(
  robots({
    UserAgent: '*',
    Disallow: '/',
    CrawlDelay: '5',
    Sitemap: 'https://nowhere.com/sitemap.xml'
  })
)

app.use(function (req, res, next) {
  const startHrTime = process.hrtime()
  res.on('finish', () => {
    const elapsedHrTime = process.hrtime(startHrTime)
    const elapsedTimeInMs = elapsedHrTime[0] * 1000 + elapsedHrTime[1] / 1e6

    console.log('-------')
    console.log('%s : %fms', req.path, elapsedTimeInMs)
    console.log(`queryParams: ${JSON.stringify(req.query, null, 2)}`)
    console.log('-------')
  })
  next()
})

app.use(cors())

const files = fg.sync(__basePath)
files.forEach((entry) => delayedGet(app, entry))

app.listen(ENV_PORT)

console.log('')
console.log(`# STARTED ON PORT :  ${ENV_PORT}`)
console.log('')
