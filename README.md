# mockly
## description
* easy express server for mocking api data
  * filebased 
    * add .json in /routes folder
    * path of json file within dir represents route
      * /routes/pages/homepage.json -> /pages/homepage
  * ability to delay response
    * add property "__delayed" (ms) to .json
      * `{"__delayed"  : 1000, ..}` 



## installation

```sh
# installxw
npm i
# starts the server, with filewatcher
npm run start
```  
  
## .env 
* create .env from .env.tpl

```properties
#port
PORT=3000
#default request delay
REQUEST_DELAY=0
#compress request using gzip
COMPRESS=true
```
#### example for request ur data width fetch(js)/curl
```js
const options = {method: 'GET'};

fetch('http://localhost:<PORT><ROUTE>', options)
  .then(response => response.json())
  .then(response => console.log(response))
  .catch(err => console.error(err));
```
```shell
curl --request GET \
  --url http://localhost:<PORT><ROUTE>
```

---
### `<PORT>` 
* default port 3000, can be overwritten in /.env 

--- 
### `<ROUTE>`  
 
* add json file in the corresponding file structure. 
route represents the relative path.   
for example: 
/routes/pages/homepage.json -> /pages/homepage
  
