import path from "path";
import { fileURLToPath } from "url";
import { outputFileSync } from "fs-extra/esm";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const __routesPath = path.normalize(__dirname + "/routes");
const __basePath = path.normalize(__routesPath + "/example/test.json");
try {
    outputFileSync(__basePath, JSON.stringify({
        __delayed: 3000
    }, null, 2))
    console.log(__basePath + ' added')
} catch (e) {
    console.error(e)
}

